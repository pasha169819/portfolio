Задачи взяты с сайта sql-ex.ru

Схема БД состоит из четырех таблиц:
Product(maker, model, type)
PC(code, model, speed, ram, hd, cd, price)
Laptop(code, model, speed, ram, hd, price, screen)
Printer(code, model, color, type, price)
Таблица Product представляет производителя (maker), номер модели (model) и тип ('PC' - ПК, 'Laptop' - ПК-блокнот или 'Printer' - принтер). 
Предполагается, что номера моделей в таблице Product уникальны для всех производителей и типов продуктов. В таблице PC для каждого ПК, однозначно 
определяемого уникальным кодом – code, указаны модель – model (внешний ключ к таблице Product), скорость - speed (процессора в мегагерцах), объем 
памяти - ram (в мегабайтах), размер диска - hd (в гигабайтах), скорость считывающего устройства - cd (например, '4x') и цена - price. Таблица 
Laptop аналогична таблице РС за исключением того, что вместо скорости CD содержит размер экрана -screen (в дюймах). В таблице Printer для каждой 
модели принтера указывается, является ли он цветным - color ('y', если цветной), тип принтера - type (лазерный – 'Laser', струйный – 'Jet' или 
матричный – 'Matrix') и цена - price.

1. Найдите номер модели, скорость и размер жесткого диска для всех ПК стоимостью менее 500 дол. Вывести: model, speed и hd:

SELECT model, speed, hd FROM pc 
WHERE price < 500

2. Найдите производителей принтеров. Вывести: maker:

SELECT DISTINCT maker FROM product 
WHERE type = 'Printer'

3. Найдите номер модели, объем памяти и размеры экранов ПК-блокнотов, цена которых превышает 1000 дол.

SELECT model, ram, screen FROM Laptop
WHERE price > 1000
 
4. Найдите все записи таблицы Printer для цветных принтеров.

SELECT * FROM Printer 
where color = 'y'
 
5. Найдите номер модели, скорость и размер жесткого диска ПК, имеющих 12x или 24x CD и цену менее 600 дол.

SELECT model, speed, hd FROM PC
WHERE cd IN ('12x', '24x') and price < 600

6. Для каждого производителя, выпускающего ПК-блокноты c объёмом жесткого диска не менее 10 Гбайт, найти скорости таких ПК-блокнотов. Вывод: производитель, скорость.

SELECT DISTINCT Product.Maker, Laptop.Speed FROM Product, Laptop
WHERE Product.type='Laptop' and Laptop.hd >=10 and Product.model=Laptop.Model
 
7. Найдите номера моделей и цены всех имеющихся в продаже продуктов (любого типа) производителя B (латинская буква).

SELECT Product.Model, PC.Price FROM Product, PC
WHERE Product.Maker = 'B' and Product.model = PC.model
UNION
SELECT Product.Model, Laptop.Price FROM Product, Laptop
WHERE Product.Maker = 'B' and Product.model = Laptop.model
UNION
SELECT Product.Model, Printer.Price FROM Product, Printer
WHERE Product.Maker = 'B' and Product.model = Printer.mode

8. Найдите производителя, выпускающего ПК, но не ПК-блокноты.

SELECT PR.Maker FROM Product PR, PC
WHERE PR.model=PC.Model
EXCEPT
SELECT PR.Maker FROM Product PR, Laptop
WHERE PR.model=Laptop.Model

9. Найдите производителей ПК с процессором не менее 450 Мгц. Вывести: Maker

SELECT DISTICT Product.maker FROM Product, PC 
WHERE PC.speed >=450 and Product.model=PC.Model

10. Найдите модели принтеров, имеющих самую высокую цену. Вывести: model, price

SELCET model,price FROM printer
WHERE price = (SELECT MAX(price) FROM printer)

11. Найдите среднюю скорость ПК.

SELECT AVG(speed) FROM PCd

12. Найдите среднюю скорость ПК-блокнотов, цена которых превышает 1000 дол.

SELECT AVG(speed) FROM Laptop
WHERE price > 1000

13. Найдите среднюю скорость ПК, выпущенных производителем A.

SELECT AVG(speed) FROM PC INNER JOIN Product 
IN PC.model = Product.Model
WHERE Product.maker = 'A'

14. Найдите класс, имя и страну для кораблей из таблицы Ships, имеющих не менее 10 орудий.

SELECT s.class, s.name, cl.country FROM ships s  JOIN Classes cl
ON s.class=cl.class
WHERE numGuns >=10

15. Найдите размеры жестких дисков, совпадающих у двух и более PC. Вывести: HD

SELECT hd FROM PC
GROUP by hd
HAVING COUNT(hd)>1

16. Найдите пары моделей PC, имеющих одинаковые скорость и RAM. В результате каждая пара указывается только один раз, т.е. (i,j), но не (j,i), Порядок вывода: модель с большим номером, модель с меньшим номером, скорость и RAM.

SELECT P.model, C.model, P.speed, P.ram FROM PC P,PC C 
WHERE P.speed = C.speed and P.RAM=C.RAM and P.model > C.model

17. Найдите модели ПК-блокнотов, скорость которых меньше скорости каждого из ПК.
Вывести: type, model, speed
 
Select Distinct PR.type,L.model,L.speed from Laptop L, PC, Product PR
Where PR.model=L.Model and L.speed < (select min(speed) from PC)

18. Найдите производителей самых дешевых цветных принтеров. Вывести: maker, price

SELECT P.maker, Pr.price from Product P,Printer Pr
WHERE P.model=Pr.model 
and Pr.color = 'y' and Pr.price = (SELECT MIN(price) FROM Printer WHERE color = 'y')

19. Для каждого производителя, имеющего модели в таблице Laptop, найдите средний размер экрана выпускаемых им ПК-блокнотов.
Вывести: maker, средний размер экрана.

SELECT PR.maker, avg(L.screen) FROM Product PR, Laptop L
WHERE PR.model = L.model
GROUP BY PR.maker

20. Найдите производителей, выпускающих по меньшей мере три различных модели ПК. Вывести: Maker, число моделей ПК.

SELECT maker, COUNT(model) AS Count_model FROM Product\
WHERE type='PC'\
GROUP BY maker\
HAVING COUNT(DISTINCT model)>2

21. Найдите максимальную цену ПК, выпускаемых каждым производителем, у которого есть модели в таблице PC.
Вывести: maker, максимальная цена.

SELECT Pr.maker, MAX(PC.price) FROM Product PR JOIN 
ON PC.model=Pr.model
GROUP BY Pr.maker

22. Для каждого значения скорости ПК, превышающего 600 МГц, определите среднюю цену ПК с такой же скоростью. Вывести: speed, средняя цена.

SELECT speed, AVG(price) AS Avg_price FROM PC
WHERE speed>600
GROUP BY speed

23. Найдите производителей, которые производили бы как ПК
со скоростью не менее 750 МГц, так и ПК-блокноты со скоростью не менее 750 МГц.
Вывести: Maker
 
SELECT Pr.maker FROM Product Pr, PC
WHERE PC.speed >= 750 and Pr.model = PC.model
INTERSECT
SELECT Pr.maker FROM Product Pr , Laptop L
WHERE L.speed >= 750 and Pr.model = L.model

24. Перечислите номера моделей любых типов, имеющих самую высокую цену по всей имеющейся в базе данных продукции.
  
SELECT model FROM 
(SELECT model,price FROM PC
UNION 
SELECT model,price FROM Laptop
UNION
SELECT model,price FROM Printer) t1
WHERE price = (SELECT MAX(price) from 
(SELECT price FROM PC
UNION
SELECT price FROM Laptop
UNION 
SELECT Price FROM Printer)t2)
 
25. Найдите производителей принтеров, которые производят ПК с наименьшим объемом RAM и с самым быстрым процессором среди всех ПК, имеющих наименьший объем RAM. Вывести: Maker
 
SELECT DISTICT maker
FROM Product
WHERE model IN 
(SELECT model FROM PC
WHERE speed = (Select MAX(speed) FROM PC
WHERE Ram = (SELECT MIN(RAM) FROM PC))
and RAM = (SELECT MIN(RAM) FROM PC))
and maker IN 
(SELECT maker FROM Product
WHERE type='printer')

